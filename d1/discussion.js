// SECTION Comparison query operators

// $gt/$gte operator
/**
 * allows us to find documents that have field number values greater than or greater than or equal to a specified number
 * syntax:
 *      db.collectionName.find({field: {$gt:value}});
 *      db.collectionName.find({field: {$gte:value}});
 */
db.users.find({age:{$gt:50}});
db.users.find({age:{$gte:50}});

// $lt/$lte operator
/**
 * allows us to find documents that have field number values less than or less than equal to a specified number
 * syntax:
 *      db.collectionName.find({field: {$lt:value}});
 *      db.collectionName.find({field: {$lte:value}});
 */
 db.users.find({age:{$lt:50}});
 db.users.find({age:{$lte:50}});

//  $ne operator
/**
 * allows us to find documents that have field number values not equal to a specified number
 * syntax:
 *      db.collectionName.find({field: {$ne:value}});
 */
 db.users.find({age:{$ne:50}});

//  $in operator
/**
 * allows us to find documents with a specific match criteria on one field using different values
 * syntax:
 *      db.collectionName.find({field: {$in:value}});
 */
 db.users.find({lastName:{$in:["Hawking","Doe"]}});
 db.users.find({courses:{$in:["HTML","React"]}});

//  SECTION - LOGICAL QUERY operators
// $or operator
/**
 * allows us to find documents matching a single criteria from multiple provided search criteria
 * syntax:
 *      db.collectionName.find({$or:[ {fieldA: valueA},{fieldB: valueB}]});
 */

 db.users.find({$or:[{firstName: "Neil"},{age: 21}]});
 db.users.find({ $or: [{ firstName: "Neil" }, { age: { $gt: 21 } }] });

 // $or operator
/**
 * allows us to find documents matching multiple criteria in a single field
 * syntax:
 *      db.collectionName.find({$and:[ {fieldA: valueA},{fieldB: valueB}]});
 */
 db.users.find({$and:[{age: {$ne: 82}}, {age: {$ne: 76}}]});

//  SECTION - field projection
/**
 * retrieving documents are common operations that we do and by default MongoDB queries return the whole document as a response.
 * when dealing with complex data strcutures there might be instances when fields are not useful for the query that we are trying to accomplish.
 * to help with readablity of the values returned, we can include/exclude fields from the response
 */
// INCLUSION
db.users.find(
    {
        firstName: "Jane"
    },
    {
        firstName: 1,
        lastName: 1,
        contact: 1
    }
);
// EXCLUSION
/**
 * allows us to exclude specific fields only when retrieving documents 
 * the value provided is 0 to denote that the field is being excluded
 * syntax:
 *      db.collectionName.find({$and:[ {fieldA: valueA},{fieldB: valueB}]});
 */
 db.users.find(
    {
        firstName: "Jane"
    },
    {
        contact: 0,
        deparment: 0
    }
);
db.users.find(
    {    },
    {
        _id: false,
        age: false,
        courses: false,
        department: false,
        nameArr: false
        
    }
);
db.users.find(
    {    },
    {
        _id: 0,
        firstName: 1,
        lastName: 1,
        contact: 1
        
    }
);
db.users.find(
    {    },
    {
        firstName: 1,
        lastName: 1,
        deparment: 0
        
    }
);
// one way of coding it
db.users.find(
    {firstName:"Jane"},
    {
        firstName: 1,
        lastName: 1,
        contact: {
            phone: 1
        }
        
    }
);
// one way of coding it
db.users.find(
    {firstName:"Jane"},
    {
        firstName: 1,
        lastName: 1,
        "contact.phone": 1
        
    }
);

// supressing
db.users.find(
    {firstName:"Jane"},
    {
        "contact.phone": 0
        
    }
);
db.users.find(
    {firstName:"Jane"},
    {
        contact: {phone:0}
    }
);

// PROJECT Specific array elements in the returned array
// $slice operator - allows us to retrieve only 1 element that matches the search criteria
db.users.find(
    {
        "nameArr":{nameA:"juan"},
    },
    {
        "nameArr":{$slice:1}
    }
)
// SECTION - Evaluation query operators
/**
 * $regex operator
 * allows us to find documents that match a specific string pattern using regular expression
 * syntax:
 *      db.collectionName.find({field: {$regex:"pattern",$options:"$optionsValue"}});
 * 
 */
// case sensitive
db.users.find({firstName:{$regex:"N"}});
// case insensitive query
db.users.find({firstName:{$regex:"n",$options:"$i"}});

db.users.find({$or:[{firstName: { $regex: "n"}},{firstName: { $regex: "N"}}]});

"https://www.w3schools.com/jsref/jsref_obj_regexp.asp"
